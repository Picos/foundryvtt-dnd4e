import {ClassFeatures} from "./classFeatures.js"

// Namespace Configuration Values
export const DND4E = {};

// ASCII Artwork
DND4E.ASCII = `_______________________________
______      ______ _____ _____
|  _  \\___  |  _  \\  ___|  ___|
| | | ( _ ) | | | |___ \\| |__
| | | / _ \\/\\ | | |   \\ \\  __|
| |/ / (_>  < |/ //\\__/ / |___
|___/ \\___/\\/___/ \\____/\\____/
_______________________________`;


/**
 * The set of Ability Scores used within the system
 * @type {Object}
 */
DND4E.abilities = {
  "str": "DND4E.AbilityStr",
  "dex": "DND4E.AbilityDex",
  "con": "DND4E.AbilityCon",
  "int": "DND4E.AbilityInt",
  "wis": "DND4E.AbilityWis",
  "cha": "DND4E.AbilityCha"
};

DND4E.abilityAbbreviations = {
  "str": "DND4E.AbilityStrAbbr",
  "dex": "DND4E.AbilityDexAbbr",
  "con": "DND4E.AbilityConAbbr",
  "int": "DND4E.AbilityIntAbbr",
  "wis": "DND4E.AbilityWisAbbr",
  "cha": "DND4E.AbilityChaAbbr"
};

/* -------------------------------------------- */

/**
 * Character alignment options
 * @type {Object}
 */
DND4E.alignments = {
  'lg': "DND4E.AlignmentLG",
  'ng': "DND4E.AlignmentNG",
  'cg': "DND4E.AlignmentCG",
  'ln': "DND4E.AlignmentLN",
  'tn': "DND4E.AlignmentTN",
  'cn': "DND4E.AlignmentCN",
  'le': "DND4E.AlignmentLE",
  'ne': "DND4E.AlignmentNE",
  'ce': "DND4E.AlignmentCE"
};


DND4E.weaponProficiencies = {
  "sim": "DND4E.WeaponSimpleProficiency",
  "mar": "DND4E.WeaponMartialProficiency"
};

DND4E.toolProficiencies = {
  "art": "DND4E.ToolArtisans",
  "disg": "DND4E.ToolDisguiseKit",
  "forg": "DND4E.ToolForgeryKit",
  "game": "DND4E.ToolGamingSet",
  "herb": "DND4E.ToolHerbalismKit",
  "music": "DND4E.ToolMusicalInstrument",
  "navg": "DND4E.ToolNavigators",
  "pois": "DND4E.ToolPoisonersKit",
  "thief": "DND4E.ToolThieves",
  "vehicle": "DND4E.ToolVehicle"
};


/* -------------------------------------------- */

/**
 * This Object defines the various lengths of time which can occur
 * @type {Object}
 */
DND4E.timePeriods = {
  "inst": "DND4E.TimeInst",
  "turn": "DND4E.TimeTurn",
  "round": "DND4E.TimeRound",
  "minute": "DND4E.TimeMinute",
  "hour": "DND4E.TimeHour",
  "day": "DND4E.TimeDay",
  "month": "DND4E.TimeMonth",
  "year": "DND4E.TimeYear",
  "perm": "DND4E.TimePerm",
  "spec": "DND4E.Special"
};


/* -------------------------------------------- */

/**
 * This describes the ways that an ability can be activated
 * @type {Object}
 */
DND4E.abilityActivationTypes = {
  "none": "DND4E.None",
  "action": "DND4E.Action",
  "bonus": "DND4E.BonusAction",
  "reaction": "DND4E.Reaction",
  "minute": DND4E.timePeriods.minute,
  "hour": DND4E.timePeriods.hour,
  "day": DND4E.timePeriods.day,
  "special": DND4E.timePeriods.spec,
  "legendary": "DND4E.LegAct",
  "lair": "DND4E.LairAct",
  "crew": "DND4E.VehicleCrewAction"
};

/* -------------------------------------------- */


DND4E.abilityConsumptionTypes = {
  "ammo": "DND4E.ConsumeAmmunition",
  "attribute": "DND4E.ConsumeAttribute",
  "material": "DND4E.ConsumeMaterial",
  "charges": "DND4E.ConsumeCharges"
};


/* -------------------------------------------- */

// Creature Sizes
DND4E.actorSizes = {
  "tiny": "DND4E.SizeTiny",
  "sm": "DND4E.SizeSmall",
  "med": "DND4E.SizeMedium",
  "lg": "DND4E.SizeLarge",
  "huge": "DND4E.SizeHuge",
  "grg": "DND4E.SizeGargantuan"
};

DND4E.tokenSizes = {
  "tiny": 1,
  "sm": 1,
  "med": 1,
  "lg": 2,
  "huge": 3,
  "grg": 4
};

/* -------------------------------------------- */

/**
 * Classification types for item action types
 * @type {Object}
 */
DND4E.itemActionTypes = {
  "mwak": "DND4E.ActionMWAK",
  "rwak": "DND4E.ActionRWAK",
  "msak": "DND4E.ActionMSAK",
  "rsak": "DND4E.ActionRSAK",
  "save": "DND4E.ActionSave",
  "heal": "DND4E.ActionHeal",
  "abil": "DND4E.ActionAbil",
  "util": "DND4E.ActionUtil",
  "other": "DND4E.ActionOther"
};

/* -------------------------------------------- */

DND4E.itemCapacityTypes = {
  "items": "DND4E.ItemContainerCapacityItems",
  "weight": "DND4E.ItemContainerCapacityWeight"
};

/* -------------------------------------------- */

/**
 * Enumerate the lengths of time over which an item can have limited use ability
 * @type {Object}
 */
DND4E.limitedUsePeriods = {
  "sr": "DND4E.ShortRest",
  "lr": "DND4E.LongRest",
  "day": "DND4E.Day",
  "charges": "DND4E.Charges"
};


/* -------------------------------------------- */

/**
 * The set of equipment types for armor, clothing, and other objects which can ber worn by the character
 * @type {Object}
 */
DND4E.equipmentTypes = {
  "light": "DND4E.EquipmentLight",
  "medium": "DND4E.EquipmentMedium",
  "heavy": "DND4E.EquipmentHeavy",
  "bonus": "DND4E.EquipmentBonus",
  "natural": "DND4E.EquipmentNatural",
  "shield": "DND4E.EquipmentShield",
  "clothing": "DND4E.EquipmentClothing",
  "trinket": "DND4E.EquipmentTrinket",
  "vehicle": "DND4E.EquipmentVehicle"
};


/* -------------------------------------------- */

/**
 * The set of Armor Proficiencies which a character may have
 * @type {Object}
 */
DND4E.armorProficiencies = {
  "lgt": DND4E.equipmentTypes.light,
  "med": DND4E.equipmentTypes.medium,
  "hvy": DND4E.equipmentTypes.heavy,
  "shl": "DND4E.EquipmentShieldProficiency"
};


/* -------------------------------------------- */

/**
 * Enumerate the valid consumable types which are recognized by the system
 * @type {Object}
 */
DND4E.consumableTypes = {
  "ammo": "DND4E.ConsumableAmmunition",
  "potion": "DND4E.ConsumablePotion",
  "poison": "DND4E.ConsumablePoison",
  "food": "DND4E.ConsumableFood",
  "scroll": "DND4E.ConsumableScroll",
  "wand": "DND4E.ConsumableWand",
  "rod": "DND4E.ConsumableRod",
  "trinket": "DND4E.ConsumableTrinket"
};

/* -------------------------------------------- */

/**
 * The valid currency denominations supported by the 4e system
 * @type {Object}
 */
DND4E.currencies = {
  "pp": "DND4E.CurrencyPP",
  "gp": "DND4E.CurrencyGP",
  "ep": "DND4E.CurrencyEP",
  "sp": "DND4E.CurrencySP",
  "cp": "DND4E.CurrencyCP",
};


/**
 * Define the upwards-conversion rules for registered currency types
 * @type {{string, object}}
 */
DND4E.currencyConversion = {
  cp: {into: "sp", each: 10},
  sp: {into: "ep", each: 5 },
  ep: {into: "gp", each: 2 },
  gp: {into: "pp", each: 10}
};

/* -------------------------------------------- */


// Damage Types
DND4E.damageTypes = {
  "acid": "DND4E.DamageAcid",
  "bludgeoning": "DND4E.DamageBludgeoning",
  "cold": "DND4E.DamageCold",
  "fire": "DND4E.DamageFire",
  "force": "DND4E.DamageForce",
  "lightning": "DND4E.DamageLightning",
  "necrotic": "DND4E.DamageNecrotic",
  "piercing": "DND4E.DamagePiercing",
  "poison": "DND4E.DamagePoison",
  "psychic": "DND4E.DamagePsychic",
  "radiant": "DND4E.DamageRadiant",
  "slashing": "DND4E.DamageSlashing",
  "thunder": "DND4E.DamageThunder"
};

// Damage Resistance Types
DND4E.damageResistanceTypes = mergeObject(duplicate(DND4E.damageTypes), {
  "physical": "DND4E.DamagePhysical"
});


/* -------------------------------------------- */

DND4E.distanceUnits = {
  "none": "DND4E.None",
  "self": "DND4E.DistSelf",
  "touch": "DND4E.DistTouch",
  "ft": "DND4E.DistFt",
  "mi": "DND4E.DistMi",
  "spec": "DND4E.Special",
  "any": "DND4E.DistAny"
};

/* -------------------------------------------- */


/**
 * Configure aspects of encumbrance calculation so that it could be configured by modules
 * @type {Object}
 */
DND4E.encumbrance = {
  currencyPerWeight: 50,
  strMultiplier: 15,
  vehicleWeightMultiplier: 2000 // 2000 lbs in a ton
};

/* -------------------------------------------- */

/**
 * This Object defines the types of single or area targets which can be applied
 * @type {Object}
 */
DND4E.targetTypes = {
  "none": "DND4E.None",
  "self": "DND4E.TargetSelf",
  "creature": "DND4E.TargetCreature",
  "ally": "DND4E.TargetAlly",
  "enemy": "DND4E.TargetEnemy",
  "object": "DND4E.TargetObject",
  "space": "DND4E.TargetSpace",
  "radius": "DND4E.TargetRadius",
  "sphere": "DND4E.TargetSphere",
  "cylinder": "DND4E.TargetCylinder",
  "cone": "DND4E.TargetCone",
  "square": "DND4E.TargetSquare",
  "cube": "DND4E.TargetCube",
  "line": "DND4E.TargetLine",
  "wall": "DND4E.TargetWall"
};


/* -------------------------------------------- */


/**
 * Map the subset of target types which produce a template area of effect
 * The keys are DND4E target types and the values are MeasuredTemplate shape types
 * @type {Object}
 */
DND4E.areaTargetTypes = {
  cone: "cone",
  cube: "rect",
  cylinder: "circle",
  line: "ray",
  radius: "circle",
  sphere: "circle",
  square: "rect",
  wall: "ray"
};


/* -------------------------------------------- */

// Healing Types
DND4E.healingTypes = {
  "healing": "DND4E.Healing",
  "temphp": "DND4E.HealingTemp"
};


/* -------------------------------------------- */


/**
 * Enumerate the denominations of hit dice which can apply to classes
 * @type {Array.<string>}
 */
DND4E.hitDieTypes = ["d6", "d8", "d10", "d12"];


/* -------------------------------------------- */

/**
 * Character senses options
 * @type {Object}
 */
DND4E.senses = {
  "bs": "DND4E.SenseBS",
  "dv": "DND4E.SenseDV",
  "ts": "DND4E.SenseTS",
  "tr": "DND4E.SenseTR"
};


/* -------------------------------------------- */

/**
 * The set of skill which can be trained
 * @type {Object}
 */
DND4E.skills = {
  "acr": "DND4E.SkillAcr",
  "arc": "DND4E.SkillArc",
  "ath": "DND4E.SkillAth",
  "blu": "DND4E.SkillBlu",
  "dip": "DND4E.SkillDip",
  "dun": "DND4E.SkillDun",
  "end": "DND4E.SkillEnd",
  "hea": "DND4E.SkillHea",
  "his": "DND4E.SkillHis",
  "ins": "DND4E.SkillIns",
  "itm": "DND4E.SkillItm",
  "nat": "DND4E.SkillNat",
  "prc": "DND4E.SkillPrc",
  "rel": "DND4E.SkillRel",
  "ste": "DND4E.SkillSte",
  "str": "DND4E.SkillStr",
  "thi": "DND4E.SkillThi"
};


/* -------------------------------------------- */

DND4E.spellPreparationModes = {
  "always": "DND4E.SpellPrepAlways",
  "atwill": "DND4E.SpellPrepAtWill",
  "innate": "DND4E.SpellPrepInnate",
  "pact": "DND4E.PactMagic",
  "prepared": "DND4E.SpellPrepPrepared"
};

DND4E.spellUpcastModes = ["always", "pact", "prepared"];


DND4E.spellProgression = {
  "none": "DND4E.SpellNone",
  "full": "DND4E.SpellProgFull",
  "half": "DND4E.SpellProgHalf",
  "third": "DND4E.SpellProgThird",
  "pact": "DND4E.SpellProgPact",
  "artificer": "DND4E.SpellProgArt"
};

/* -------------------------------------------- */

/**
 * The available choices for how spell damage scaling may be computed
 * @type {Object}
 */
DND4E.spellScalingModes = {
  "none": "DND4E.SpellNone",
  "cantrip": "DND4E.SpellCantrip",
  "level": "DND4E.SpellLevel"
};

/* -------------------------------------------- */


/**
 * Define the set of types which a weapon item can take
 * @type {Object}
 */
DND4E.weaponTypes = {
  "simpleM": "DND4E.WeaponSimpleM",
  "simpleR": "DND4E.WeaponSimpleR",
  "martialM": "DND4E.WeaponMartialM",
  "martialR": "DND4E.WeaponMartialR",
  "natural": "DND4E.WeaponNatural",
  "improv": "DND4E.WeaponImprov",
  "siege": "DND4E.WeaponSiege"
};


/* -------------------------------------------- */

/**
 * Define the set of weapon property flags which can exist on a weapon
 * @type {Object}
 */
DND4E.weaponProperties = {
  "amm": "DND4E.WeaponPropertiesAmm",
  "hvy": "DND4E.WeaponPropertiesHvy",
  "fin": "DND4E.WeaponPropertiesFin",
  "fir": "DND4E.WeaponPropertiesFir",
  "foc": "DND4E.WeaponPropertiesFoc",
  "lgt": "DND4E.WeaponPropertiesLgt",
  "lod": "DND4E.WeaponPropertiesLod",
  "rch": "DND4E.WeaponPropertiesRch",
  "rel": "DND4E.WeaponPropertiesRel",
  "ret": "DND4E.WeaponPropertiesRet",
  "spc": "DND4E.WeaponPropertiesSpc",
  "thr": "DND4E.WeaponPropertiesThr",
  "two": "DND4E.WeaponPropertiesTwo",
  "ver": "DND4E.WeaponPropertiesVer"
};


// Spell Components
DND4E.spellComponents = {
  "V": "DND4E.ComponentVerbal",
  "S": "DND4E.ComponentSomatic",
  "M": "DND4E.ComponentMaterial"
};

// Spell Schools
DND4E.spellSchools = {
  "abj": "DND4E.SchoolAbj",
  "con": "DND4E.SchoolCon",
  "div": "DND4E.SchoolDiv",
  "enc": "DND4E.SchoolEnc",
  "evo": "DND4E.SchoolEvo",
  "ill": "DND4E.SchoolIll",
  "nec": "DND4E.SchoolNec",
  "trs": "DND4E.SchoolTrs"
};

// Spell Levels
DND4E.spellLevels = {
  0: "DND4E.SpellLevel0",
  1: "DND4E.SpellLevel1",
  2: "DND4E.SpellLevel2",
  3: "DND4E.SpellLevel3",
  4: "DND4E.SpellLevel4",
  5: "DND4E.SpellLevel5",
  6: "DND4E.SpellLevel6",
  7: "DND4E.SpellLevel7",
  8: "DND4E.SpellLevel8",
  9: "DND4E.SpellLevel9"
};

// Spell Scroll Compendium UUIDs
DND4E.spellScrollIds = {
  0: 'Compendium.dnd4e.items.rQ6sO7HDWzqMhSI3',
  1: 'Compendium.dnd4e.items.9GSfMg0VOA2b4uFN',
  2: 'Compendium.dnd4e.items.XdDp6CKh9qEvPTuS',
  3: 'Compendium.dnd4e.items.hqVKZie7x9w3Kqds',
  4: 'Compendium.dnd4e.items.DM7hzgL836ZyUFB1',
  5: 'Compendium.dnd4e.items.wa1VF8TXHmkrrR35',
  6: 'Compendium.dnd4e.items.tI3rWx4bxefNCexS',
  7: 'Compendium.dnd4e.items.mtyw4NS1s7j2EJaD',
  8: 'Compendium.dnd4e.items.aOrinPg7yuDZEuWr',
  9: 'Compendium.dnd4e.items.O4YbkJkLlnsgUszZ'
};

/**
 * Define the standard slot progression by character level.
 * The entries of this array represent the spell slot progression for a full spell-caster.
 * @type {Array[]}
 */
DND4E.SPELL_SLOT_TABLE = [
  [2],
  [3],
  [4, 2],
  [4, 3],
  [4, 3, 2],
  [4, 3, 3],
  [4, 3, 3, 1],
  [4, 3, 3, 2],
  [4, 3, 3, 3, 1],
  [4, 3, 3, 3, 2],
  [4, 3, 3, 3, 2, 1],
  [4, 3, 3, 3, 2, 1],
  [4, 3, 3, 3, 2, 1, 1],
  [4, 3, 3, 3, 2, 1, 1],
  [4, 3, 3, 3, 2, 1, 1, 1],
  [4, 3, 3, 3, 2, 1, 1, 1],
  [4, 3, 3, 3, 2, 1, 1, 1, 1],
  [4, 3, 3, 3, 3, 1, 1, 1, 1],
  [4, 3, 3, 3, 3, 2, 1, 1, 1],
  [4, 3, 3, 3, 3, 2, 2, 1, 1]
];

/* -------------------------------------------- */

// Polymorph options.
DND4E.polymorphSettings = {
  keepPhysical: 'DND4E.PolymorphKeepPhysical',
  keepMental: 'DND4E.PolymorphKeepMental',
  keepSaves: 'DND4E.PolymorphKeepSaves',
  keepSkills: 'DND4E.PolymorphKeepSkills',
  mergeSaves: 'DND4E.PolymorphMergeSaves',
  mergeSkills: 'DND4E.PolymorphMergeSkills',
  keepClass: 'DND4E.PolymorphKeepClass',
  keepFeats: 'DND4E.PolymorphKeepFeats',
  keepSpells: 'DND4E.PolymorphKeepSpells',
  keepItems: 'DND4E.PolymorphKeepItems',
  keepBio: 'DND4E.PolymorphKeepBio',
  keepVision: 'DND4E.PolymorphKeepVision'
};

/* -------------------------------------------- */

/**
 * Skill, ability, and tool proficiency levels
 * Each level provides a proficiency multiplier
 * @type {Object}
 */
DND4E.proficiencyLevels = {
  0: "DND4E.NotProficient",
  1: "DND4E.Proficient",
  0.5: "DND4E.HalfProficient",
  2: "DND4E.Expertise"
};

/* -------------------------------------------- */

/**
 * The amount of cover provided by an object.
 * In cases where multiple pieces of cover are
 * in play, we take the highest value.
 */
DND4E.cover = {
  0: 'DND4E.None',
  .5: 'DND4E.CoverHalf',
  .75: 'DND4E.CoverThreeQuarters',
  1: 'DND4E.CoverTotal'
};

/* -------------------------------------------- */


// Condition Types
DND4E.conditionTypes = {
  "blinded": "DND4E.ConBlinded",
  "charmed": "DND4E.ConCharmed",
  "deafened": "DND4E.ConDeafened",
  "diseased": "DND4E.ConDiseased",
  "exhaustion": "DND4E.ConExhaustion",
  "frightened": "DND4E.ConFrightened",
  "grappled": "DND4E.ConGrappled",
  "incapacitated": "DND4E.ConIncapacitated",
  "invisible": "DND4E.ConInvisible",
  "paralyzed": "DND4E.ConParalyzed",
  "petrified": "DND4E.ConPetrified",
  "poisoned": "DND4E.ConPoisoned",
  "prone": "DND4E.ConProne",
  "restrained": "DND4E.ConRestrained",
  "stunned": "DND4E.ConStunned",
  "unconscious": "DND4E.ConUnconscious"
};

// Languages
DND4E.languages = {
  "common": "DND4E.LanguagesCommon",
  "aarakocra": "DND4E.LanguagesAarakocra",
  "abyssal": "DND4E.LanguagesAbyssal",
  "aquan": "DND4E.LanguagesAquan",
  "auran": "DND4E.LanguagesAuran",
  "celestial": "DND4E.LanguagesCelestial",
  "deep": "DND4E.LanguagesDeepSpeech",
  "draconic": "DND4E.LanguagesDraconic",
  "druidic": "DND4E.LanguagesDruidic",
  "dwarvish": "DND4E.LanguagesDwarvish",
  "elvish": "DND4E.LanguagesElvish",
  "giant": "DND4E.LanguagesGiant",
  "gith": "DND4E.LanguagesGith",
  "gnomish": "DND4E.LanguagesGnomish",
  "goblin": "DND4E.LanguagesGoblin",
  "gnoll": "DND4E.LanguagesGnoll",
  "halfling": "DND4E.LanguagesHalfling",
  "ignan": "DND4E.LanguagesIgnan",
  "infernal": "DND4E.LanguagesInfernal",
  "orc": "DND4E.LanguagesOrc",
  "primordial": "DND4E.LanguagesPrimordial",
  "sylvan": "DND4E.LanguagesSylvan",
  "terran": "DND4E.LanguagesTerran",
  "cant": "DND4E.LanguagesThievesCant",
  "undercommon": "DND4E.LanguagesUndercommon"
};

// Character Level XP Requirements
DND4E.CHARACTER_EXP_LEVELS =  [
  0, 300, 900, 2700, 6500, 14000, 23000, 34000, 48000, 64000, 85000, 100000,
  120000, 140000, 165000, 195000, 225000, 265000, 305000, 355000]
;

// Challenge Rating XP Levels
DND4E.CR_EXP_LEVELS = [
  10, 200, 450, 700, 1100, 1800, 2300, 2900, 3900, 5000, 5900, 7200, 8400, 10000, 11500, 13000, 15000, 18000,
  20000, 22000, 25000, 33000, 41000, 50000, 62000, 75000, 90000, 105000, 120000, 135000, 155000
];

// Character Features Per Class And Level
DND4E.classFeatures = ClassFeatures;

// Configure Optional Character Flags
DND4E.characterFlags = {
  "powerfulBuild": {
    name: "DND4E.FlagsPowerfulBuild",
    hint: "DND4E.FlagsPowerfulBuildHint",
    section: "Racial Traits",
    type: Boolean
  },
  "savageAttacks": {
    name: "DND4E.FlagsSavageAttacks",
    hint: "DND4E.FlagsSavageAttacksHint",
    section: "Racial Traits",
    type: Boolean
  },
  "elvenAccuracy": {
    name: "DND4E.FlagsElvenAccuracy",
    hint: "DND4E.FlagsElvenAccuracyHint",
    section: "Racial Traits",
    type: Boolean
  },
  "halflingLucky": {
    name: "DND4E.FlagsHalflingLucky",
    hint: "DND4E.FlagsHalflingLuckyHint",
    section: "Racial Traits",
    type: Boolean
  },
  "initiativeAdv": {
    name: "DND4E.FlagsInitiativeAdv",
    hint: "DND4E.FlagsInitiativeAdvHint",
    section: "Feats",
    type: Boolean
  },
  "initiativeAlert": {
    name: "DND4E.FlagsAlert",
    hint: "DND4E.FlagsAlertHint",
    section: "Feats",
    type: Boolean
  },
  "jackOfAllTrades": {
    name: "DND4E.FlagsJOAT",
    hint: "DND4E.FlagsJOATHint",
    section: "Feats",
    type: Boolean
  },
  "observantFeat": {
    name: "DND4E.FlagsObservant",
    hint: "DND4E.FlagsObservantHint",
    skills: ['prc','inv'],
    section: "Feats",
    type: Boolean
  },
  "reliableTalent": {
    name: "DND4E.FlagsReliableTalent",
    hint: "DND4E.FlagsReliableTalentHint",
    section: "Feats",
    type: Boolean
  },
  "remarkableAthlete": {
    name: "DND4E.FlagsRemarkableAthlete",
    hint: "DND4E.FlagsRemarkableAthleteHint",
    abilities: ['str','dex','con'],
    section: "Feats",
    type: Boolean
  },
  "weaponCriticalThreshold": {
    name: "DND4E.FlagsCritThreshold",
    hint: "DND4E.FlagsCritThresholdHint",
    section: "Feats",
    type: Number,
    placeholder: 20
  }
};

// Configure allowed status flags
DND4E.allowedActorFlags = [
  "isPolymorphed", "originalActor"
].concat(Object.keys(DND4E.characterFlags));

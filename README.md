# Foundry Virtual Tabletop - DnD4e Game System

## THIS IS A FORK FROM dnd5e UNDER HEAVY DEVELOPMENT. DO NOT USE UNLESS YOU KNOW WHAT YOU DO.

This game system for [Foundry Virtual Tabletop](http://foundryvtt.com) provides character sheet and game system 
support for the Fifth Edition of the world's most popular roleplaying game.

This system provides character sheet support for Actors and Items, mechanical support for dice and rules necessary to
play games of 5th Edition, and compendium content for Monsters, Heroes, Items, Spells, Class Features, Monster 
Features, and more!

The software component of this system is distributed under the GNUv3 license.

## Installation Instructions

To install and use the DnD4e system for Foundry Virtual Tabletop, simply paste the following URL into the 
**Install System** dialog on the Setup menu of the application.

https://gitlab.com/Picos/foundryvtt-dnd4e/raw/master/system.json

If you wish to manually install the system, you must clone or extract it into the ``Data/systems/dnd4e`` folder. You
may do this by cloning the repository or downloading a zip archive from the
[Releases Page](https://gitlab.com/Picos/foundryvtt-dnd4e/-/releases).

## Community Contribution

Code and content contributions are accepted. Please feel free to submit issues to the issue tracker or submit merge
requests for code changes. Approval for such requests involves code and (if necessary) design review by Atropos. Please
reach out on the Foundry Community Discord with any questions.